﻿using PipeStreams;
using System;

namespace PipeServer
{

    class ServerJson
    {
        public static bool isConnected = false;
        public static void Main(string[] args)
        {
            using (PipeWriterString writer = new PipeWriterString("ServerClientPipeString"))
            {
                writer.PipeWrittenStringEvent += Writer_PipeWritten;
                writer.PipeConnected += Writer_PipeConnected;
                writer.PipeDisconnected += Writer_PipeDisconnected;
                string message = "";
                do
                {
                    if (isConnected)
                    {
                        Console.WriteLine("write a message to send to pipe:");
                        message = Console.ReadLine();
                        if (message == "q")
                        {
                            break;
                        }
                        writer.Write(message);
                    }
                } while (message != "q"); //will never be reached, can be replaced with while(true)
            }//using
            _ = Console.ReadLine();
        }//main  

        private static void Writer_PipeDisconnected()
        {
            isConnected = false;
        }

        private static void Writer_PipeConnected()
        {
            isConnected = true;
        }

        private static void Writer_PipeWritten(string message, int bytesWritten)
        {
            Console.Write(message.ToCharArray(), 0, bytesWritten);
        }
    }//class
}//namespace

