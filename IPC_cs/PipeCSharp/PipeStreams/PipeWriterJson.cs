﻿using System;
using System.Collections.Generic;
using System.Text.Json;

namespace PipeStreams
{
    public class PipeWriterJson : PipeWriterString
    {

        #region events
        public event PipeWriteHandle<string> PipeWrittenJsonEvent;
        #endregion

        #region properties
        Type classType;
        public JsonSerializerOptions Options { get; set; }
        private readonly string SPLITTING_CHAR = "@@";
        private Dictionary<Type, TypeNameEnum> typeToTypeEnum;
        #endregion

        /// <summary>
        /// creates a Writer thas sends objects through the pipe using JSON
        /// </summary>
        /// <param name="pipeName">the name of the named pipe to connect to</param>
        /// <param name="bufferSize">(optional) the max size of the messege to send</param>
        public PipeWriterJson(string pipeName, int bufferSize = 4096) : base(pipeName, bufferSize)
        {
            Options = new JsonSerializerOptions
            {
                WriteIndented = true,
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };
            InitializeDictionary();
        }
        /// <summary>
        /// this function sends an object through the pipe.
        /// </summary>
        /// <typeparam name="T">the type of the object to send</typeparam>
        /// <param name="objectToSend">the object to send</param>
        public void Write<T>(T objectToSend)
        {
            classType = typeof(T);
            string jsonString = WriteMessage(objectToSend);
            PipeWrittenJsonEvent?.Invoke(jsonString, jsonString.Length);
            base.Write(jsonString);
            return;
        }
        private string WriteMessage(object objectToSend)
        {
            string jsonString = string.Format("{0}", (int)typeToTypeEnum[classType]);
            jsonString += SPLITTING_CHAR;//these should be seperating charachters that won`t be a part of the json or the TypeName
            jsonString += SerializeMessage(objectToSend, classType, Options);
            return jsonString;
        }

        private string SerializeMessage(object objectToSend, Type type, JsonSerializerOptions options)
        {
            return JsonSerializer.Serialize(objectToSend, type, options);
        }

        /// <summary>
        /// updates the JSON options from the default
        /// </summary>
        /// <param name="options">the new options</param>
        public void UpdateWriterOptions(JsonSerializerOptions options)
        {
            Options = options;
        }
        #region initialize dictionary
        private static bool isInitialized = false;
        private void InitializeDictionary()
        {
            if (!isInitialized)
            {
                isInitialized = true;
                typeToTypeEnum = new Dictionary<Type, TypeNameEnum>
                {
                    { typeof(string), TypeNameEnum.STRING },
                    { typeof(string[]), TypeNameEnum.STRING_ARRAY },
                    { typeof(int), TypeNameEnum.INT },
                    { typeof(int[]), TypeNameEnum.INT_ARRAY },
                    { typeof(char), TypeNameEnum.CHAR },
                    { typeof(char[]), TypeNameEnum.CHAR_ARRAY },
                    { null , TypeNameEnum.UNKNOWN },
                    { typeof(WeatherForecast), TypeNameEnum.WEATHER_FORECAST },
                    { typeof(TestClass),  TypeNameEnum.TEST_CLASS }
                };
            }
        }
        #endregion
        public override void Dispose()
        {
            base.Dispose();
        }
    }
}
