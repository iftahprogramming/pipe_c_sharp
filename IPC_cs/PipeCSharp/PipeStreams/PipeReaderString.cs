﻿using System.Text;

namespace PipeStreams
{
    public class PipeReaderString : PipeReaderByte
    {
        #region events
        public new event PipeReadHandler<string> PipeReadEvent;
        #endregion
        /// <summary>
        /// creates a reader that recives string message from the pipe
        /// </summary>
        /// <param name="pipeName">the name of the named pipe to connect to</param>
        /// <param name="bufferSize">(optional)the max size of the message to send</param>
        public PipeReaderString(string pipeName, int bufferSize = 4096) : base(pipeName, bufferSize)
        {
            base.PipeReadEvent += PipeReaderString_PipeReadEvent;
        }

        private void PipeReaderString_PipeReadEvent(byte[] encodedMessage, int bytesRead)
        {
            PipeReadEvent?.Invoke(Encoding.Default.GetString(encodedMessage,0,bytesRead).Substring(0, bytesRead), bytesRead); // !!! Check
        }
        /// <summary>
        /// Starts to listen for messages from the server
        /// </summary>
        public override void StartReceive()
        {
            base.StartReceive();
        }//StartRead
        /// <summary>
        /// Stop listening for messgaes 
        /// </summary>
        public override void StopReceive()
        {
            base.StopReceive();
        }

        public override void Dispose()
        {
            base.Dispose();
        }

    }//class
}//namespace
