﻿using System;
using System.IO;
using System.IO.Pipes;

public class PipeReader : StreamReader
{

	//public delegate void PipeReadHandler();

	public event EventHandler PipeRead;	

	public PipeReader(NamedPipeClientStream pipeClientStream) : base(pipeClientStream) { }
	//public PipeReader(NamedPipeServerStream pipeServerStream) : base(pipeServerStream) { }

	public string PipeReadLine()
    {
		PipeRead.Invoke(this, EventArgs.Empty);
		return ReadLine();
    }


}//class
