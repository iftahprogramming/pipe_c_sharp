﻿using System;
using System.IO;
using System.IO.Pipes;

public class PipeClient : NamedPipeClientStream 
{
    #region c`tors
    public PipeClient(string pipeName):base(pipeName){ }
	public PipeClient(string serverName, string pipeName):base(serverName, pipeName) { }
	public PipeClient(string serverName, string pipeName, PipeDirection direction) :base(serverName, pipeName, direction) { }
	public PipeClient(string serverName, string pipeName, PipeDirection direction, PipeOptions options) : base(serverName, pipeName, direction, options) { }
    #endregion
}//class

//private string pipeClientName;
//public string PipeClientName
//   {
//	get { return pipeClientName; }
//       set { this.pipeClientName = value; }
//   }
//private string pipeServerName;
//public string PipeServerName
//   {
//	get { return pipeServerName; }
//       set { this.pipeServerName = value; }
//   }
//private NamedPipeClientStream client;
//private NamedPipeServerStream server;


////c`tor
//public Pipe(string pipeClientName, string pipeServerName)
//{
//	PipeClientName = pipeClientName;
//	PipeServerName = pipeServerName;

//	this.client = null;
//	this.server = null;
//}//pipe

///// <summary>
//   /// tries to conect to client and server pipes based on given pipe names
//   /// </summary>
//   /// <returns></returns>
//public bool ConnectToPipe()
//{
//	try 
//	{
//		this.client = new NamedPipeClientStream(".", pipeClientName, PipeDirection.InOut);
//		client.Connect(2000); //try to connect to a server with that name for 2 seconds
//		this.server = new NamedPipeServerStream(PipeServerName, PipeDirection.InOut);
//		server.WaitForConnection();
//	}
//	catch(IOException e)
//	{
//		return false;
//	}

//	catch(SystemException e)
//       {
//		return false;
//       }

//	return (this.client != null || this.server != null);

//}//ConnectToPipe


///// <summary>
//   /// this function attempts to connect to server and client pipes based on a given name
//   /// </summary>
//   /// <param name="pipeClientName">name of the client pipe</param>
//   /// <param name="pipeServerName">name of the server pipe</param>
//   /// <returns></returns>
//public bool ConnectToPipe(string pipeClientName, string pipeServerName)
//   {
//	try
//	{
//		this.client = new NamedPipeClientStream(".", pipeClientName, PipeDirection.InOut);
//		client.Connect(2000); //try to connect to a server with that name for 2 seconds
//		this.server = new NamedPipeServerStream(pipeServerName, PipeDirection.InOut);
//		server.WaitForConnection();
//	}
//	catch (IOException e)
//	{
//		return false;
//	}

//	catch (SystemException e)
//	{
//		if (e == System.TimeoutException)
//		{
//			return false;
//		}
//		if (e == System.InvalidOperationException)
//		{
//			return false;
//		}
//	}

//	return (this.client != null || this.server != null);
//}

///// <summary>
///// This function attempts to connect to an existing pipe and if it fails, it creates one.
///// </summary>
///// <returns>
///// true - if the pipe was created/if the connection to the server was successful
///// false - if the both the creation and connection failed
///// </returns>
//public bool ConnectToPipe(string pipeName)
//   {
//	try
//	{
//		this.client = new NamedPipeClientStream(".", pipeName, PipeDirection.InOut);
//		client.Connect(2000); //try to connect to a server with that name for 2 seconds

//	}
//	catch (IOException e)
//	{
//		return false;
//	}

//	catch (SystemException e)
//	{
//		if(e == System.TimeoutException)
//           {
//			this.client = null;
//			this.server = new NamedPipeServerStream(pipeName, PipeDirection.InOut);
//			server.WaitForConnection();
//		}

//	}

//	return (client != null || server != null);
//}

//public void SendStringToPipe(string messege)
//   {

//   }