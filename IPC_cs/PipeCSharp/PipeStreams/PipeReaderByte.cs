﻿using System;
using System.IO;
using System.Threading;

namespace PipeStreams
{
    public class PipeReaderByte : PipeReader
    {
        #region events
        public event PipeReadHandler<byte[]> PipeReadEvent;
        #endregion

        #region properties
        private readonly BufferedStream clientStream;
        byte[] buffer;
        #endregion
        /// <summary>
        /// creates a reader that recives byte array from the pipe
        /// </summary>
        /// <param name="pipeName">the name of the named pipe to connect to</param>
        /// <param name="bufferSize">(optional)the max size of the message to send</param>
        public PipeReaderByte(string pipeName, int bufferSize = 4096) : base(pipeName, bufferSize)
        {
            clientStream = new BufferedStream(clientPipe);
            buffer = new byte[bufferSize];
        }

        /// <summary>
        /// Starts to listen for messages from the server
        /// </summary>
        public override void StartReceive()
        {
            new Thread(() =>
            {
                while (isRunning && clientPipe.IsConnected)
                {
                    int bytesRead = clientStream.Read(buffer, 0, buffer.Length);
                    PipeReadEvent?.Invoke(buffer, bytesRead);
                    Array.Clear(buffer, 0, bytesRead);
                }
            }).Start();
        }//StartReceive

        /// <summary>
        /// Stop listening for messgaes 
        /// </summary>
        public override void StopReceive()
        {
            base.StopReceive();
        }

        public override void Dispose()
        {
            clientStream.Close();
            base.Dispose();
        }


    }//class
}//namespace
