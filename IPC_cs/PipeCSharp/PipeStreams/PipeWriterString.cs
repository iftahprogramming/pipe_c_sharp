﻿using System.Text;

namespace PipeStreams
{
    public class PipeWriterString : PipeWriterByte
    {


        #region events
        public event PipeWriteHandle<string> PipeWrittenStringEvent;
        #endregion
        /// <summary>
        /// creates a writer that sends string messeges them through the pipe
        /// </summary>
        /// <param name="pipeName">the name of the named pipe to connect to</param>
        /// <param name="bufferSize">(optional) the max size of the messege to send</param>
        public PipeWriterString(string pipeName, int bufferSize = 4096) : base(pipeName, bufferSize)
        {

        }
        /// <summary>
        /// writes a byte array message to the pipes
        /// </summary>
        /// <param name="message">the message to send through the pipe</param>
        public void Write(string message)
        {
            base.Write(Encoding.Default.GetBytes(message));
            PipeWrittenStringEvent?.Invoke(message, message.Length);
        }

        public override void Dispose()
        {
            base.Dispose();
        }
    }
}
