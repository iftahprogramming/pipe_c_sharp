﻿using System;
using System.Text;
using System.Text.Json;
using PipeStreams;

namespace PipeClient
{
    class ClientByte
    {
        static void Main(string[] args)
        {
            using (PipeReaderByte reader = new PipeReaderByte("ServerClientPipeByte"))
            {
                reader.PipeReadEvent += SrClient_PipeReadEvent;
                reader.StartReceive();
                string temp;
                if ((temp = Console.ReadLine()) == "q")
                    reader.StopReceive();
            }//using 
            Console.ReadLine();


        }//main

        private static void SrClient_PipeReadEvent(byte[] buffer, int bytesRead)
        {
            Console.WriteLine(Encoding.Default.GetString(buffer), 0, bytesRead);
        }
    }//class
}//namespace