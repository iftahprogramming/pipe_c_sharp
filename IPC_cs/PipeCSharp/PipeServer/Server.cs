﻿using PipeStreams;
using System;
using System.Text;
using System.Text.Json;

namespace PipeServer
{
    public class WeatherForecast
    {
        public DateTimeOffset Date { get; set; }
        public int TemperatureCelsius { get; set; }
        public string Summary { get; set; }
    }
    class Server
    {
        static void Main(string[] args)
        {
                JsonSerializerOptions options = new JsonSerializerOptions
                {
                    WriteIndented = true,
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase
                };
                WeatherForecast weatherForecast = new WeatherForecast
                {
                    Date = DateTimeOffset.Now,
                    TemperatureCelsius = 20,
                    Summary = "nice"
                };
            using (PipeWriterJson writer = new PipeWriterJson("ServerClientPipe", typeof(WeatherForecast), options))
            {
                writer.PipeWrittenEvent += Writer_PipeWritten;

                writer.PipeWrite(weatherForecast);
            }//using
            _ = Console.ReadLine();
        }//main  

        private static void Writer_PipeWritten(string buffer, int bytesWritten)
        {
            Console.WriteLine(buffer);
        }
    }//class
}//namespace

