﻿using PipeStreams;
using System;
using System.Collections.Generic;

namespace PipeServer
{

    class ServerJson
    {
        public static bool isConnected = false;
        public static void Main(string[] args)
        {
            WeatherForecast weatherForecast = new WeatherForecast
            {
                Date = DateTimeOffset.Now,
                TemperatureCelsius = new int[] { 20, 10, 5 },
                Summary = new List<string> { "nice", "cold", "warm" },
                Test = new TestClass { Name = "hello" }
            };
            using (PipeWriterJson writer = new PipeWriterJson("ServerClientPipeJson"))
            {
                writer.PipeWrittenJsonEvent += Writer_PipeWritten;
                writer.PipeConnected += Writer_PipeConnected;
                writer.PipeDisconnected += Writer_PipeDisconnected;
                do
                {
                    if (isConnected)
                    {
                        writer.Write(weatherForecast);
                        break;
                    }
                } while (!isConnected);

            }//using
            _ = Console.ReadLine();
        }//main  

        private static void Writer_PipeDisconnected()
        {
            isConnected = false;
        }

        private static void Writer_PipeConnected()
        {
            isConnected = true;
        }

        private static void Writer_PipeWritten(string buffer, int bytesWritten)
        {
            Console.WriteLine(buffer);
        }
    }//class
}//namespace

