# IPC in c# (or PipeCSharp)

this is a mentorship project written in c# by Adar Poliak.

## Getting Started

The in order to start a pipe you have to create a PipeReader on the client process and a PipeWriter on the server process.  You have to give both the Writer and Reader the **exact same name**  
(to the letter!!! or else they wil not connect). The reader has a function called Write to wich you pass the message you want to send through the pipe.  
for example, lets say want to send a string messege through the pipe. the server side will have:  
```C#  
PiprWriterString writer = new PiprWriterString("pipeName");  
writer.Write("hello world!");  
```  
the client side will have:  
```C#  
PipeReaderString reader = new PipeReaderString("pipeName");  
reader.StartReceive();  
```  
The reader will listen for a message from the pipe and will raise a PipeWrittenStringEvent witch will have a string message and a int bytesRead(the length of the message).  
```C#  
reader.PipeReadEvent += SrClient_PipeReadEvent;  
...  
static void SrClient_PipeReadEvent(string message, int bytesRead)  
    {  
        Console.WriteLine(message);       
    }  
```  
For further examples, see the example folder in the solution.  
* if you want to use the pipe as a full duplex you should add both a reader and a writer to both projects and essantialy create two pipes  
### Prerequisites  

there are no prerequisites for this project  

### Installing  

In order to install the project to your program you should add the project as a reference and include it.  
```C#  
using PipeStreams  
```  
After you have done so, you can use the relevant pipe reader\writer.  

## Running the tests  

In the solution there is a folder called "examples". Inside the folder there are projects for each type of pipe client and server the program supports(byte, string and JSON).   
in order to test the project You need to start both .exe files of the server and the client and follow the instructions written in the program.  
for example:  
```s   
enter a mesage to send through the pipe:  
```  
In that case you should type the message you want to send.  

## Authors  

* **Adar Poliak** - *mentorship project* -   
