﻿using System;
using System.Text;
using PipeStreams;

namespace PipeClient
{
    class ClientByte
    {
        static void Main(string[] args)
        {
            using (PipeReaderByte reader = new PipeReaderByte("ServerClientPipeByte"))
            {
                reader.PipeReadEvent += SrClient_PipeReadEvent;
                reader.StartReceive();
                string temp;
                if ((temp = Console.ReadLine()) == "q")
                    reader.StopReceive();
            }//using 
            Console.ReadLine();


        }//main

        private static void SrClient_PipeReadEvent(byte[] buffer, int bytesRead)
        {
            Console.Write(Encoding.Default.GetString(buffer).ToCharArray(), 0, bytesRead);
            Console.WriteLine();
        }
    }//class
}//namespace