﻿using PipeStreams;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace PipeServer
{

    class ServerJson
    {
        public static void Main(string[] args)
        {
            WeatherForecast weatherForecast = new WeatherForecast
            {
                Date = DateTimeOffset.Now,
                TemperatureCelsius = new int[] { 20, 10, 5 },
                Summary = new List<string> { "nice", "cold", "warm" },
                Test = new TestClass { Name = "hello" }
            };
            using (PipeWriterJson writer = new PipeWriterJson("ServerClientPipe"))
            {
                writer.PipeWrittenJsonEvent += Writer_PipeWritten;
                Console.WriteLine(weatherForecast.Test.Name);
                writer.Write(weatherForecast);
            }//using
            _ = Console.ReadLine();
        }//main  

        private static void Writer_PipeWritten(string buffer, int bytesWritten)
        {
            Console.WriteLine(buffer);
        }
    }//class
}//namespace

