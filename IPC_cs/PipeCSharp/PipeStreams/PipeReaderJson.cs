﻿using System;
using System.Text.Json;
using System.Collections.Generic;

namespace PipeStreams
{
    #region test classes
    public class TestClass
    {
        public string Name { get; set; }
    }
    public class WeatherForecast
    {
        public DateTimeOffset Date { get; set; }
        public int[] TemperatureCelsius { get; set; }
        public List<string> Summary { get; set; }
        public TestClass Test { get; set; }
    }
    #endregion

    #region enums
    public enum TypeNameEnum
    {
        UNKNOWN = -1,
        INT = 1,
        INT_ARRAY = 2,
        STRING = 3,
        STRING_ARRAY = 4,
        CHAR = 5,
        CHAR_ARRAY = 6,
        WEATHER_FORECAST = 7,
        TEST_CLASS = 8
    }
    public enum MessageIndexes
    {
        TYPE = 0,
        JSON_STRING = 1
    }

    #endregion

    public class PipeReaderJson<T> : PipeReaderString
    {

        private readonly string SPLITTING_CHAR = "@@";

        #region events and delegates
        public event PipeReadHandler<object> PipeReadJsonEvent;
        public event ErrorMessageHandle<ErrorTypes> Error;
        #endregion

        #region fields
        private JsonSerializerOptions Options { get; set; }
        private Dictionary<TypeNameEnum, Type> typeEnumToType;
        #endregion

        #region properties
        public Type Type { get; set; }
        #endregion
        /// <summary>
        /// creates a reader that recives objects from the pipe using JSON
        /// </summary>
        /// <param name="pipeName">the name of the named pipe to connect to</param>
        /// <param name="bufferSize">(optional)the max size of the message to send</param>
        public PipeReaderJson(string pipeName, int bufferSize = 4096) : base(pipeName, bufferSize)
        {
            Options = new JsonSerializerOptions
            {
                WriteIndented = true,
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };
            InitializeDictionary();
            base.PipeReadEvent += PipeReaderJson_PipeReadEvent;
        }

        private void PipeReaderJson_PipeReadEvent(string buffer, int bytesRead)
        {
            string[] typeAndJson = buffer.Split(SPLITTING_CHAR, 2); //split the sent messege into two by the splitting char
            if (typeAndJson.Length != 2)
                return; // illegal input
            try // if the type dosen`t exist in the dictionarry an exception is thrown
            {
                TypeNameEnum type = (TypeNameEnum)int.Parse(typeAndJson[(int)MessageIndexes.TYPE]);
                if (typeEnumToType.ContainsKey(type))
                    Type = typeEnumToType[type];
                else
                {
                    Type = typeEnumToType[TypeNameEnum.UNKNOWN];
                    Error?.Invoke(ErrorTypes.INVALID_TYPE, "Could not find the type of the sent object.");
                    return;
                }
            }
            catch
            {
                return;
            }

            object objectSent = null;
            if (bytesRead > 0)
            {
                string jsonString = typeAndJson[(int)MessageIndexes.JSON_STRING].Substring(0, bytesRead - (typeAndJson[(int)MessageIndexes.TYPE].Length + 2));//typeAndJson[0].Length + 1 is to get the normal string and another +1 to remove the \0
                try
                {
                    objectSent = DeserializeMessage(jsonString, Type, Options);
                }
                catch (JsonException e) //StackTrace
                {
                    Error?.Invoke(ErrorTypes.INVALID_JSON, "Invalid JSON Content.");
                    return;
                }
                PipeReadJsonEvent?.Invoke(objectSent, bytesRead);
            }
            else //if nothing was read (TimeOut)
            {
                Error?.Invoke(ErrorTypes.TIME_OUT, "No message was sent.");
            }

        }//PipeReaderJson_PipeReadEvent

        /// <summary>
        /// Start listening for messages from the server
        /// </summary>
        public override void StartReceive()
        {
            base.StartReceive();
        }//StartReceive

        /// <summary>
        /// Stop listening for messgaes 
        /// </summary>
        public override void StopReceive()
        {
            base.StopReceive();
        }
        private object DeserializeMessage(string message, Type type, JsonSerializerOptions options = null)
        {
            return JsonSerializer.Deserialize(message, type, options);
        }

        #region initialize dictionary
        private static bool isInitialized = false;
        private void InitializeDictionary()
        {
            if (!isInitialized)
            {
                isInitialized = true;
                typeEnumToType = new Dictionary<TypeNameEnum, Type>();
                typeEnumToType.Add(TypeNameEnum.STRING, typeof(string));
                typeEnumToType.Add(TypeNameEnum.STRING_ARRAY, typeof(string[]));
                typeEnumToType.Add(TypeNameEnum.INT, typeof(int));
                typeEnumToType.Add(TypeNameEnum.INT_ARRAY, typeof(int[]));
                typeEnumToType.Add(TypeNameEnum.CHAR, typeof(char));
                typeEnumToType.Add(TypeNameEnum.CHAR_ARRAY, typeof(char[]));
                typeEnumToType.Add(TypeNameEnum.UNKNOWN, null);
                typeEnumToType.Add(TypeNameEnum.WEATHER_FORECAST, typeof(WeatherForecast));
                typeEnumToType.Add(TypeNameEnum.TEST_CLASS, typeof(TestClass));

                // Self Check. ASSERT
                foreach (TypeNameEnum value in Enum.GetValues(typeof(TypeNameEnum)))
                {
                    if (!typeEnumToType.ContainsKey(value))
                    {

                    }
                }

            }
        }
        #endregion

        public override void Dispose()
        {
            base.Dispose();
        }
    }
}
