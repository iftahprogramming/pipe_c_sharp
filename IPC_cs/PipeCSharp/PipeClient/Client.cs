﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Threading;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using PipeStreams;

namespace PipeClient
{
    public class WeatherForecast
    {
        public DateTimeOffset Date { get; set; }
        public int TemperatureCelsius { get; set; }
        public string Summary { get; set; }
    }
    class Client
    {
        static void Main(string[] args)
        {
            using (PipeReaderJson reader = new PipeReaderJson("ServerClientPipe", typeof(WeatherForecast)))
            {
                reader.PipeReadEvent += SrClient_PipeReadEvent;
                reader.StartReceive();
                string temp;
                if ((temp = Console.ReadLine()) == "q")
                    reader.StopReceive();
            }//using 
            Console.ReadLine();


        }//main

        private static void SrClient_PipeReadEvent(object buffer, int bytesRead)
        {
            Console.WriteLine("message recived");
        }
    }//class
}//namespace