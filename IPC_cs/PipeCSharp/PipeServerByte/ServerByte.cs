﻿using PipeStreams;
using System;
using System.Text;

namespace PipeServer
{

    class ServerJson
    {
        public static bool isConected = false;
        public static void Main(string[] args)
        {
            using (PipeWriterByte writer = new PipeWriterByte("ServerClientPipeByte"))
            {
                writer.PipeWrittenBytesEvent += Writer_PipeWritten;
                writer.PipeConnected += Writer_PipeConnected;
                writer.PipeDisconnected += Writer_PipeDisconnected;
                
                string message = "";
                do
                {
                    if (isConected)
                    {
                        Console.WriteLine("write a message to send through the pipe:");
                        message = Console.ReadLine();
                        if (message == "q")
                        {
                            break;
                        }
                        writer.Write(Encoding.Default.GetBytes(message));
                    }
                } while (message != "q"); //will never be reached, can be replaced with while(true)
            }//using
            _ = Console.ReadLine();
        }//main  

        private static void Writer_PipeDisconnected()
        {
            isConected = false;
        }

        private static void Writer_PipeConnected()
        {
            isConected = true;
        }
        private static void Writer_PipeWritten(byte[] message, int bytesWritten)
        {
            Console.Write(Encoding.Default.GetString(message), 0, bytesWritten);
        }
    }//class
}//namespace

                
