﻿using System;
using System.Text.Json;
using PipeStreams;

namespace PipeClient
{
    class ClientJson
    {
        static void Main(string[] args)
        {
            using (PipeReaderJson<WeatherForecast> reader = new PipeReaderJson<WeatherForecast>("ServerClientPipeJson"))
            {
                reader.PipeReadEvent += SrClient_PipeReadEvent;
                reader.StartReceive();
                string temp;
                if ((temp = Console.ReadLine()) == "q")
                    reader.StopReceive();
            }//using 
            Console.ReadLine();


        }//main

        private static void SrClient_PipeReadEvent(object buffer, int bytesRead)
        {
            Console.WriteLine("message recived");
        }
    }//class
}//namespace