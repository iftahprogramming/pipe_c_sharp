﻿using System;
using PipeStreams;

namespace PipeClient
{
    class ClientString
    {
        static void Main(string[] args)
        {
            using (PipeReaderString reader = new PipeReaderString("ServerClientPipeString"))
            {
                reader.PipeReadEvent += SrClient_PipeReadEvent;
                reader.StartReceive();
                string temp;
                if ((temp = Console.ReadLine()) == "q")
                    reader.StopReceive();
            }//using 
            _ = Console.ReadLine();
        }//main

        private static void SrClient_PipeReadEvent(string message, int bytesRead)
        {
            Console.WriteLine(message);
        }
    }//class
}//namespace