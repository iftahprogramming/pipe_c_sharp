﻿using System;
using System.IO;
using System.IO.Pipes;

public class PipeWriter : StreamWriter
{

    public event EventHandler PipeWritten;
	public PipeWriter(NamedPipeServerStream serverStream):base(serverStream){ }

	public void PipeWrite(char value)
    {
        PipeWritten.Invoke(this, EventArgs.Empty);
        Write(value);
    }
}
