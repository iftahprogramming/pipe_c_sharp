﻿using System;
using System.IO.Pipes;
using System.Threading.Tasks;

public abstract class PipeWriter<T> : IDisposable
{
    #region delegates and events
    public delegate void PipeWriteHandle<TBufferType>(TBufferType buffer, int bytesWritten);
    public delegate void PipeConnectedHandle();
    public delegate void PipeDisconnectedHandle();
    public delegate void ErrorMessageHandle<TErrorType>(TErrorType errorType, string message);

    public event PipeConnectedHandle PipeConnected;
    public event PipeDisconnectedHandle PipeDisconnected;

    #endregion

    #region properties
    protected NamedPipeServerStream serverPipe;
    #endregion

    public PipeWriter(string pipeName)
    {
        try
        {
            serverPipe = new NamedPipeServerStream(pipeName, PipeDirection.InOut);
            _ = WaitForConnectionAsync();
        }
        catch (System.ObjectDisposedException) //this exception is thrown if the pipe is closed 
        {
            serverPipe = null;
            PipeDisconnected?.Invoke();
        }
        catch (System.InvalidOperationException)//this exception is thrown if the pipe already exists
        {
            PipeDisconnected?.Invoke();
        }

    }

    public async Task WaitForConnectionAsync()
    {
        Task connectTask = serverPipe.WaitForConnectionAsync();
        await connectTask;
        PipeConnected?.Invoke();
    }


    /// <summary>
    /// writes a byte array message to the pipes
    /// </summary>
    /// <param name="message">the message to send through the pipe</param>
    public abstract void Write(T message);

    protected abstract bool CheckMessageBeforeSending(T message);

    public virtual void Dispose()
    {
        PipeDisconnected?.Invoke();
        serverPipe.Close();
    }
}
