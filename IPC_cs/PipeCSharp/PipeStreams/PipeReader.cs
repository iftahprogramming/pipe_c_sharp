﻿using System;
using System.IO.Pipes;
using System.Threading.Tasks;

public abstract class PipeReader : IDisposable
{
    #region delegates and events
    public delegate void PipeReadHandler<T>(T buffer, int bytesRead);
    public delegate void PipeConnectedHandler();
    public delegate void PipeDisconnectedHandler();
    public delegate void ErrorMessageHandle<TErrorType>(TErrorType errorType, string message);

    public event PipeConnectedHandler PipeConnectedEvent;
    public event PipeDisconnectedHandler PipeDisconnectedEvent;
    #endregion

    #region properties
    protected bool isRunning = true;
    protected NamedPipeClientStream clientPipe;

    public int BufferLength { get; }

    #endregion
    public PipeReader(string pipeName, int bufferLength)
    {
        try
        {
            clientPipe = new NamedPipeClientStream(".", pipeName, PipeDirection.InOut);
            ConnectToPipeAsync();
            PipeConnectedEvent?.Invoke();
        }
        catch
        {
            clientPipe = null;
            StopReceive();
            PipeDisconnectedEvent?.Invoke();
        }
        BufferLength = bufferLength;
    }

    public async void ConnectToPipeAsync()
    {
        Task connect = clientPipe.ConnectAsync();
        await connect;
    }


    public abstract void StartReceive();

    public virtual void StopReceive()
    {
        isRunning = false;
    }

    public virtual void Dispose()
    {
        clientPipe.Close();
        PipeDisconnectedEvent?.Invoke();
    }
}//class

