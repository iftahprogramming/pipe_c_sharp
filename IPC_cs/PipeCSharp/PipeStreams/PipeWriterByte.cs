﻿using System.IO;

namespace PipeStreams
{
    #region enums
    public enum ErrorTypes
    {
        MESSEGE_TOO_LONG,
        INVALID_TYPE,
        TIME_OUT,
        INVALID_JSON
    }
    #endregion
    public class PipeWriterByte : PipeWriter<byte[]>
    {

        #region events
        public event PipeWriteHandle<byte[]> PipeWrittenBytesEvent;
        public event ErrorMessageHandle<ErrorTypes> Error;
        #endregion

        #region properties
        readonly BufferedStream serverStream;
        readonly int bufferSize;
        #endregion

        /// <summary>
        /// creates a writer that sends byte arrays through the pipe
        /// </summary>
        /// <param name="pipeName">the name of the named pipe to connect to</param>
        /// <param name="bufferSize">(optional) the max size of the messege to send</param>
        public PipeWriterByte(string pipeName, int bufferSize = 4096) : base(pipeName)
        {
            serverStream = new BufferedStream(serverPipe);
            this.bufferSize = bufferSize;
        }

        /// <summary>
        /// writes a byte array message to the pipes
        /// </summary>
        /// <param name="message">the message to send through the pipe</param>
        public override void Write(byte[] message)
        {
            if (CheckMessageBeforeSending(message))
            {
                try
                {
                    serverStream.Write(message, 0, message.Length);
                    serverStream.Flush();
                    PipeWrittenBytesEvent?.Invoke(message, message.Length);
                }
                catch { }
            }
            else
            {
                Error?.Invoke(ErrorTypes.MESSEGE_TOO_LONG, "The messege was too long");
            }
        }
        public override void Dispose()
        {
            serverStream.Close();
            base.Dispose();
        }

        protected override bool CheckMessageBeforeSending(byte[] message)
        {
            return message.Length <= bufferSize;
        }
    }//class
}//namespace

